#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>


int main()

{
	char name[256];
	int age;
	int gender;
	int weight;
	int height;

	// getting name

	printf("Hi, what's your name? (no spaces):\n");
	scanf("%s", name);
	
	// getting age
	while(1)
	{
		printf("How old are you, %s?\n", name);
		if (scanf("%d", &age) != 1)
		{
			printf("Data error!\n");
			fflush(stdin);
			continue;
		}
		else
			break;
	}
	if (age >= 18)
		printf("Access granted!\n");
	else
		printf("Access denied!\n");

	// getting gender

	printf("What's your gender? (type '1' for male or '2' for female)\n");
	fflush(stdin);
	scanf("%d", &gender);
	
	//getting weight

	while (1)
	{
		printf("What's your weight? (type number in kilogramms)\n");
		fflush(stdin);
		if (scanf("%d", &weight) < 0)
		{
			printf("Data error! Weight cannot be negative!");
			fflush(stdin);
			continue;
		}
		else
			break;

	}
	//getting height

	while (1)
	{
		printf("What's your height? (type number in centimiters)\n");
		fflush(stdin);
		if (scanf("%d", &height) < 0)
		{
			printf("Data error! Height cannot be negative!");
			fflush(stdin);
			continue;
		}
		else
			break;

	}

	//comparing genre/weight/height

	if (gender == 1)
	{
		printf("Calculating optimal weight-height correlation\n");
		if ((height - 100) > weight)
		{
			printf("You're fat!");
			return 0;
		}
		if ((height - 100) < weight)
		{
			printf("You're too thin!");
			return 0;
		}
		if ((height - 100) == weight)
		{
			printf("You're ideal!");
			return 0;
		}
	}
	if (gender == 2)
	{
		printf("Calculating optimal weight-height correlation\n");
		if ((height - 80) > weight)
		{
			printf("You're fat!");
			return 0;
		}
		if ((height - 80) < weight)
		{
			printf("You're too thin!");
			return 0;
		}
		if ((height - 80) == weight)
		{
			printf("You're ideal!");
			return 0;
		}
	}
	else
		printf("Error");

}